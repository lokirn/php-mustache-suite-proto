<?php

    error_reporting(E_ALL);
    ini_set("display_errors", 1);

    require "vendor/autoload.php";

    $data = json_decode( file_get_contents("https://content-service.miss.test.styria-publishing.com/api/contents/1") );


    $m = new Mustache_Engine(array(
        'loader'          => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/views'),
        'partials_loader' => new Mustache_Loader_FilesystemLoader(dirname(__FILE__) . '/views/partials'),
    ));

    // add custom data
    $data->pageTitle = "mustache-frontend prototype";
    $data->imageUrl = "https://media.diepresse.com" . $data->image->uri;


    echo $m->render('page', $data); // "Hello, World!"

    // echo "<pre>" . json_encode($data) . "</pre>";
?>
